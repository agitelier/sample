package com.gitlab.agitelier.sample.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.gitlab.agitelier.sample.web.rest.TestUtil;

public class SpaceEventTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SpaceEvent.class);
        SpaceEvent spaceEvent1 = new SpaceEvent();
        spaceEvent1.setId("id1");
        SpaceEvent spaceEvent2 = new SpaceEvent();
        spaceEvent2.setId(spaceEvent1.getId());
        assertThat(spaceEvent1).isEqualTo(spaceEvent2);
        spaceEvent2.setId("id2");
        assertThat(spaceEvent1).isNotEqualTo(spaceEvent2);
        spaceEvent1.setId(null);
        assertThat(spaceEvent1).isNotEqualTo(spaceEvent2);
    }
}
