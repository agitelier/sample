import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class MissionUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('sampleApp.mission.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#mission-name'));

  descriptionInput: ElementFinder = element(by.css('input#mission-description'));
}
