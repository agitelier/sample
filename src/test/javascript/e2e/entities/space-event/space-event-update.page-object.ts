import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class SpaceEventUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('sampleApp.spaceEvent.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#space-event-name'));

  dateInput: ElementFinder = element(by.css('input#space-event-date'));

  descriptionInput: ElementFinder = element(by.css('textarea#space-event-description'));

  photoInput: ElementFinder = element(by.css('input#file_photo'));

  typeSelect = element(by.css('select#space-event-type'));
  missionSelect = element(by.css('select#space-event-mission'));
}
