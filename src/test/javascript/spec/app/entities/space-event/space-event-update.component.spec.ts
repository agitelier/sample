/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import SpaceEventUpdateComponent from '@/entities/space-event/space-event-update.vue';
import SpaceEventClass from '@/entities/space-event/space-event-update.component';
import SpaceEventService from '@/entities/space-event/space-event.service';

import MissionService from '@/entities/mission/mission.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('SpaceEvent Management Update Component', () => {
    let wrapper: Wrapper<SpaceEventClass>;
    let comp: SpaceEventClass;
    let spaceEventServiceStub: SinonStubbedInstance<SpaceEventService>;

    beforeEach(() => {
      spaceEventServiceStub = sinon.createStubInstance<SpaceEventService>(SpaceEventService);

      wrapper = shallowMount<SpaceEventClass>(SpaceEventUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          spaceEventService: () => spaceEventServiceStub,

          missionService: () => new MissionService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: '123' };
        comp.spaceEvent = entity;
        spaceEventServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(spaceEventServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.spaceEvent = entity;
        spaceEventServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(spaceEventServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
