/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import SpaceEventDetailComponent from '@/entities/space-event/space-event-details.vue';
import SpaceEventClass from '@/entities/space-event/space-event-details.component';
import SpaceEventService from '@/entities/space-event/space-event.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('SpaceEvent Management Detail Component', () => {
    let wrapper: Wrapper<SpaceEventClass>;
    let comp: SpaceEventClass;
    let spaceEventServiceStub: SinonStubbedInstance<SpaceEventService>;

    beforeEach(() => {
      spaceEventServiceStub = sinon.createStubInstance<SpaceEventService>(SpaceEventService);

      wrapper = shallowMount<SpaceEventClass>(SpaceEventDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { spaceEventService: () => spaceEventServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundSpaceEvent = { id: '123' };
        spaceEventServiceStub.find.resolves(foundSpaceEvent);

        // WHEN
        comp.retrieveSpaceEvent('123');
        await comp.$nextTick();

        // THEN
        expect(comp.spaceEvent).toBe(foundSpaceEvent);
      });
    });
  });
});
