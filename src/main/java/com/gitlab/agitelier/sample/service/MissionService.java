package com.gitlab.agitelier.sample.service;

import com.gitlab.agitelier.sample.domain.Mission;
import com.gitlab.agitelier.sample.repository.MissionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Mission}.
 */
@Service
public class MissionService {

    private final Logger log = LoggerFactory.getLogger(MissionService.class);

    private final MissionRepository missionRepository;

    public MissionService(MissionRepository missionRepository) {
        this.missionRepository = missionRepository;
    }

    /**
     * Save a mission.
     *
     * @param mission the entity to save.
     * @return the persisted entity.
     */
    public Mission save(Mission mission) {
        log.debug("Request to save Mission : {}", mission);
        return missionRepository.save(mission);
    }

    /**
     * Get all the missions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<Mission> findAll(Pageable pageable) {
        log.debug("Request to get all Missions");
        return missionRepository.findAll(pageable);
    }


    /**
     * Get one mission by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<Mission> findOne(String id) {
        log.debug("Request to get Mission : {}", id);
        return missionRepository.findById(id);
    }

    /**
     * Delete the mission by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete Mission : {}", id);
        missionRepository.deleteById(id);
    }
}
