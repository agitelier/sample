/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package com.gitlab.agitelier.sample.service.mapper;
