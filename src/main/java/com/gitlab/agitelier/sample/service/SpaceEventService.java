package com.gitlab.agitelier.sample.service;

import com.gitlab.agitelier.sample.domain.SpaceEvent;
import com.gitlab.agitelier.sample.repository.SpaceEventRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing {@link SpaceEvent}.
 */
@Service
public class SpaceEventService {

    private final Logger log = LoggerFactory.getLogger(SpaceEventService.class);

    private final SpaceEventRepository spaceEventRepository;

    public SpaceEventService(SpaceEventRepository spaceEventRepository) {
        this.spaceEventRepository = spaceEventRepository;
    }

    /**
     * Save a spaceEvent.
     *
     * @param spaceEvent the entity to save.
     * @return the persisted entity.
     */
    public SpaceEvent save(SpaceEvent spaceEvent) {
        log.debug("Request to save SpaceEvent : {}", spaceEvent);
        return spaceEventRepository.save(spaceEvent);
    }

    /**
     * Get all the spaceEvents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Page<SpaceEvent> findAll(Pageable pageable) {
        log.debug("Request to get all SpaceEvents");
        return spaceEventRepository.findAll(pageable);
    }


    /**
     * Get one spaceEvent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<SpaceEvent> findOne(String id) {
        log.debug("Request to get SpaceEvent : {}", id);
        return spaceEventRepository.findById(id);
    }

    /**
     * Delete the spaceEvent by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete SpaceEvent : {}", id);
        spaceEventRepository.deleteById(id);
    }
}
