package com.gitlab.agitelier.sample.domain.enumeration;

/**
 * The SpaceEventType enumeration.
 */
public enum SpaceEventType {
    LAUNCH, LANDING
}
