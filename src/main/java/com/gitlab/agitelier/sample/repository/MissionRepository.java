package com.gitlab.agitelier.sample.repository;

import com.gitlab.agitelier.sample.domain.Mission;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the Mission entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MissionRepository extends MongoRepository<Mission, String> {
}
