package com.gitlab.agitelier.sample.repository;

import com.gitlab.agitelier.sample.domain.SpaceEvent;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the SpaceEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SpaceEventRepository extends MongoRepository<SpaceEvent, String> {
}
