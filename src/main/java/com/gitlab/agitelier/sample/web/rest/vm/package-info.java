/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gitlab.agitelier.sample.web.rest.vm;
