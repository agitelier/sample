/**
 * Data Access Objects used by WebSocket services.
 */
package com.gitlab.agitelier.sample.web.websocket.dto;
