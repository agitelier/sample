import { IMission } from '@/shared/model/mission.model';

export const enum SpaceEventType {
  LAUNCH = 'LAUNCH',
  LANDING = 'LANDING',
}

export interface ISpaceEvent {
  id?: string;
  name?: string;
  date?: Date;
  description?: any;
  photoContentType?: string;
  photo?: any;
  type?: SpaceEventType;
  mission?: IMission;
}

export class SpaceEvent implements ISpaceEvent {
  constructor(
    public id?: string,
    public name?: string,
    public date?: Date,
    public description?: any,
    public photoContentType?: string,
    public photo?: any,
    public type?: SpaceEventType,
    public mission?: IMission
  ) {}
}
