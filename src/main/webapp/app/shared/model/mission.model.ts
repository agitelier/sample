export interface IMission {
  id?: string;
  name?: string;
  description?: string;
}

export class Mission implements IMission {
  constructor(public id?: string, public name?: string, public description?: string) {}
}
