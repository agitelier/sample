import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import MissionService from '../mission/mission.service';
import { IMission } from '@/shared/model/mission.model';

import AlertService from '@/shared/alert/alert.service';
import { ISpaceEvent, SpaceEvent } from '@/shared/model/space-event.model';
import SpaceEventService from './space-event.service';

const validations: any = {
  spaceEvent: {
    name: {
      required,
    },
    date: {
      required,
    },
    description: {
      required,
    },
    photo: {
      required,
    },
    type: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class SpaceEventUpdate extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('spaceEventService') private spaceEventService: () => SpaceEventService;
  public spaceEvent: ISpaceEvent = new SpaceEvent();

  @Inject('missionService') private missionService: () => MissionService;

  public missions: IMission[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.spaceEventId) {
        vm.retrieveSpaceEvent(to.params.spaceEventId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.spaceEvent.id) {
      this.spaceEventService()
        .update(this.spaceEvent)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('sampleApp.spaceEvent.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.spaceEventService()
        .create(this.spaceEvent)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('sampleApp.spaceEvent.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveSpaceEvent(spaceEventId): void {
    this.spaceEventService()
      .find(spaceEventId)
      .then(res => {
        this.spaceEvent = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public clearInputImage(field, fieldContentType, idInput): void {
    if (this.spaceEvent && field && fieldContentType) {
      if (this.spaceEvent.hasOwnProperty(field)) {
        this.spaceEvent[field] = null;
      }
      if (this.spaceEvent.hasOwnProperty(fieldContentType)) {
        this.spaceEvent[fieldContentType] = null;
      }
      if (idInput) {
        (<any>this).$refs[idInput] = null;
      }
    }
  }

  public initRelationships(): void {
    this.missionService()
      .retrieve()
      .then(res => {
        this.missions = res.data;
      });
  }
}
