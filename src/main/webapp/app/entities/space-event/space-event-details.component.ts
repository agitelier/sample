import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ISpaceEvent } from '@/shared/model/space-event.model';
import SpaceEventService from './space-event.service';

@Component
export default class SpaceEventDetails extends mixins(JhiDataUtils) {
  @Inject('spaceEventService') private spaceEventService: () => SpaceEventService;
  public spaceEvent: ISpaceEvent = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.spaceEventId) {
        vm.retrieveSpaceEvent(to.params.spaceEventId);
      }
    });
  }

  public retrieveSpaceEvent(spaceEventId) {
    this.spaceEventService()
      .find(spaceEventId)
      .then(res => {
        this.spaceEvent = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
