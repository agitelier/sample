import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import { IMission, Mission } from '@/shared/model/mission.model';
import MissionService from './mission.service';

const validations: any = {
  mission: {
    name: {
      required,
    },
    description: {},
  },
};

@Component({
  validations,
})
export default class MissionUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('missionService') private missionService: () => MissionService;
  public mission: IMission = new Mission();
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.missionId) {
        vm.retrieveMission(to.params.missionId);
      }
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.mission.id) {
      this.missionService()
        .update(this.mission)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('sampleApp.mission.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.missionService()
        .create(this.mission)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('sampleApp.mission.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveMission(missionId): void {
    this.missionService()
      .find(missionId)
      .then(res => {
        this.mission = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {}
}
