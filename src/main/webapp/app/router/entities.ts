import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const SpaceEvent = () => import('@/entities/space-event/space-event.vue');
// prettier-ignore
const SpaceEventUpdate = () => import('@/entities/space-event/space-event-update.vue');
// prettier-ignore
const SpaceEventDetails = () => import('@/entities/space-event/space-event-details.vue');
// prettier-ignore
const Mission = () => import('@/entities/mission/mission.vue');
// prettier-ignore
const MissionUpdate = () => import('@/entities/mission/mission-update.vue');
// prettier-ignore
const MissionDetails = () => import('@/entities/mission/mission-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default [
  {
    path: '/space-event',
    name: 'SpaceEvent',
    component: SpaceEvent,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/space-event/new',
    name: 'SpaceEventCreate',
    component: SpaceEventUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/space-event/:spaceEventId/edit',
    name: 'SpaceEventEdit',
    component: SpaceEventUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/space-event/:spaceEventId/view',
    name: 'SpaceEventView',
    component: SpaceEventDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/mission',
    name: 'Mission',
    component: Mission,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/mission/new',
    name: 'MissionCreate',
    component: MissionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/mission/:missionId/edit',
    name: 'MissionEdit',
    component: MissionUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/mission/:missionId/view',
    name: 'MissionView',
    component: MissionDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
